import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'FlutterDemo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue[800],
          title: Text('Detail Beastudi'),
          leading: Icon(Icons.keyboard_backspace),
        ),
        body: Column(children: [
          Card(
              margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
              child: ListTile(
                title: Text('Beastudi Mahasiswa Berprestasi ',
                    textAlign: (TextAlign.center),
                    style: TextStyle(
                        fontSize: 17.0, color: const Color(0xFF37aae8))),
              )),
          Container(
            child: ListTile(
                title: Text(
                  'Periode Pendaftaran :',
                  style: TextStyle(fontSize: 15.0),
                ),
                subtitle: Text(
                  '   8 Agustus 2020 s.d 8 September 2020',
                  style:
                      TextStyle(fontSize: 15.0, color: const Color(0xff000000)),
                )),
          ),
          Container(
            child: ListTile(
                title: Text(
                  'Deskripsi :',
                  style: TextStyle(fontSize: 15.0),
                ),
                subtitle: Text(
                  '   Bagi siswa/i SMA/SMK/Sederajat yang berprestasi baik dalam bidang akademik dan non-akademik dapat memanfaatkan beasiswa ini tanpa tes Ujian Saringan Masuk (USM). Beasiswa hanya diperuntukan bagi siswa/i dengan nilai rata-rata Ijazah/SKL minimal 7,5.',
                  textAlign: (TextAlign.justify),
                  style:
                      TextStyle(fontSize: 15.0, color: const Color(0xff000000)),
                )),
          ),
          SizedBox(height: 190.0),
          SizedBox(height: 10.0),
          RaisedButton(
            onPressed: () {},
            color: Colors.white,
            child: const Text('Unduh Dokumen',
                style: TextStyle(fontSize: 20, color: const Color(0xFF37aae8))),
          ),
          SizedBox(height: 5.0),
          SizedBox(height: 0.0),
          RaisedButton(
            onPressed: () {},
            color: Colors.orange[300],
            child:
                const Text('Daftar Beastudi', style: TextStyle(fontSize: 20)),
          ),
        ]),
      ),
    );
  }
}
