import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue[800],
          leading: Icon(Icons.keyboard_backspace),
          title: Text("Beastudi"),
        ),
        body: Column(
          children: [
            Card(
              margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
              child: ListTile(
                title: Text(
                  '8 Agustus 2020',
                  style: TextStyle(fontSize: 15.0),
                ),
                subtitle: Text('Beastudi Mahasiswa Berpresatasi',
                    style: TextStyle(
                        fontSize: 15.0, color: const Color(0xFF37aae8))),
              ),
            )
          ],
        ),
      ),
    );
  }
}
