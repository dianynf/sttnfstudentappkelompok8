import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(home: Layout2()));

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue[800],
          leading: Icon(Icons.keyboard_backspace),
          title: Text("Beastudi"),
        ),
        body: Column(
          children: [
            Card(
                margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Layout2()),
                    );
                  },
                  child: ListTile(
                    title: Text(
                      '8 Agustus 2020',
                      style: TextStyle(fontSize: 15.0),
                    ),
                    subtitle: Text('Beastudi Mahasiswa Berprestasi',
                        style: TextStyle(
                            fontSize: 15.0, color: const Color(0xFF37aae8))),
                  ),
                ))
          ],
        ),
      ),
    );
  }
}

class Layout2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue[800],
          leading: GestureDetector(
              onTap: () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => MyApp()));
              },
              child: Icon(Icons.keyboard_backspace)),
          title: Text('Detail Beastudi'),
        ),
        body: Column(children: [
          Card(
              margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
              child: ListTile(
                title: Text('Beastudi Mahasiswa Berprestasi ',
                    textAlign: (TextAlign.center),
                    style: TextStyle(
                        fontSize: 17.0,
                        color: Color(0xFF37aae8),
                        letterSpacing: 1.0)),
              )),
          Container(
            child: ListTile(
                title: Text(
                  'Periode Pendaftaran :',
                  style: TextStyle(fontSize: 15.0),
                ),
                subtitle: Text(
                  '   8 Agustus 2020 s.d 8 September 2020',
                  style: TextStyle(fontSize: 15.0, color: Color(0xff000000)),
                )),
          ),
          Container(
            child: ListTile(
                title: Text(
                  'Deskripsi :',
                  style: TextStyle(fontSize: 15.0),
                ),
                subtitle: Text(
                  '   Bagi siswa/i SMA/SMK/Sederajat yang berprestasi baik dalam bidang akademik dan non-akademik dapat memanfaatkan beasiswa ini tanpa tes Ujian Saringan Masuk (USM). Beasiswa hanya diperuntukan bagi siswa/i dengan nilai rata-rata Ijazah/SKL minimal 7,5.',
                  textAlign: (TextAlign.justify),
                  style: TextStyle(
                      fontSize: 15.0,
                      color: Color(0xff000000),
                      letterSpacing: 1.0),
                )),
          ),
          SizedBox(height: 80.0),
          Expanded(
              child: Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
                width: 350,
                child: OutlineButton(
                    onPressed: () {},
                    borderSide: BorderSide(color: Colors.blue),
                    textColor: Colors.blue,
                    padding: EdgeInsets.all(10.0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    child: Text("Unduh Dokumen",
                        style: TextStyle(fontSize: 20, letterSpacing: 1.00)))),
          )),
          Expanded(
              child: Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
                width: 350,
                child: RaisedButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Layout3()));
                    },
                    color: Colors.orange[700],
                    textColor: Colors.white,
                    padding: EdgeInsets.all(13.0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    child: Text("Daftar Beastudi",
                        style: TextStyle(fontSize: 20, letterSpacing: 1.00)))),
          )),
          SizedBox(height: 10.0),
        ]),
      ),
    );
  }
}

class Layout3 extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue[800],
          leading: GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Layout2()));
              },
              child: Icon(Icons.keyboard_backspace)),
          title: Text(
            "Daftar Beastudi",
            style: TextStyle(
              letterSpacing: 1.00,
            ),
          ),
        ),
        body: Column(
          children: [
            SizedBox(height: 15.0),
            Column(
              children: <Widget>[
                Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      // padding: EdgeInsets.only(left: 20),
                      child: Text("Nama",
                          style: TextStyle(fontSize: 16, color: Colors.black)),
                    )),
              ],
            ),
            SizedBox(height: 5.0),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 32.0),
                      borderRadius: BorderRadius.circular(7.0)),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 1.0),
                      borderRadius: BorderRadius.circular(7.0))),
              onChanged: (value) {
                //Do something with this value
              },
            ),
            SizedBox(height: 10.0),
            Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    // padding: EdgeInsets.only(left: 20),
                    child: Text("NIM",
                        style: TextStyle(fontSize: 16, color: Colors.black)),
                  ),
                ),
              ],
            ),
            SizedBox(height: 5.0),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 32.0),
                      borderRadius: BorderRadius.circular(5.0)),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 1.0),
                      borderRadius: BorderRadius.circular(5.0))),
              onChanged: (value) {
                //Do something with this value
              },
            ),
            SizedBox(height: 10.0),
            Column(
              children: <Widget>[
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    // padding: EdgeInsets.only(left: 20),
                    child: Text("Unggah Berkas",
                        style: TextStyle(fontSize: 14, color: Colors.black)),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10.0),
            SizedBox(
              width: 350,
              child: RaisedButton(
                  color: Colors.blue[400],
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  onPressed: () {},
                  textColor: Colors.white,
                  padding: EdgeInsets.all(10.0),
                  child: Text('Pilih Berkas',
                      style: TextStyle(fontSize: 20, letterSpacing: 1.00))),
            ),
            Expanded(
                child: Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                  width: 350,
                  child: RaisedButton(
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => MyApp()));
                      },
                      color: Colors.orange[700],
                      textColor: Colors.white,
                      padding: EdgeInsets.all(13.0),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Text("Kirim",
                          style:
                              TextStyle(fontSize: 20, letterSpacing: 1.00)))),
            )),
            SizedBox(height: 10.0),
          ],
        ),
      ),
    );
  }
}
