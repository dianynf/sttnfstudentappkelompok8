import 'package:flutter/material.dart';

void main() => runApp(Layout3());

class Layout3 extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue[800],
          leading: Icon(Icons.keyboard_backspace),
          title: Text("Daftar Beastudi"),
        ),
        body: Column(
          children: [
            SizedBox(height: 30.0),
            Text('Nama'),
            SizedBox(height: 5.0),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 32.0),
                      borderRadius: BorderRadius.circular(5.0)),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 1.0),
                      borderRadius: BorderRadius.circular(5.0))),
              onChanged: (value) {
                //Do something with this value
              },
            ),
            SizedBox(height: 10.0),
            Text('NIM'),
            SizedBox(height: 5.0),
            TextFormField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 32.0),
                      borderRadius: BorderRadius.circular(5.0)),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 1.0),
                      borderRadius: BorderRadius.circular(5.0))),
              onChanged: (value) {
                //Do something with this value
              },
            ),
            SizedBox(height: 10.0),
            Text('Unggah Berkas'),
            SizedBox(height: 5.0),
            RaisedButton(
              onPressed: () {},
              child: const Text('Pilih Berkas', style: TextStyle(fontSize: 20)),
            ),
            SizedBox(height: 60.0),
            SizedBox(height: 5.0),
            RaisedButton(
              onPressed: () {},
              child: const Text('Kirim', style: TextStyle(fontSize: 20)),
            ),
          ],
        ),
      ),
    );
  }
}
